<?php

namespace Drupal\eca_helper\Plugin\ECA\Event;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Event\Tag;
use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\eca_helper\Event\PreProcessEvent;

/**
 * Plugin implementation of the ECA Events for config.
 *
 * @EcaEvent(
 *   id = "eca_helper_preprocess_hook",
 *   deriver = "Drupal\eca_helper\Plugin\ECA\Event\PreProcessECAEventDeriver"
 * )
 */
class PreProcessECAEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    return [
      'preprocess' => [
        'label' => 'ECA Helper: Preprocess',
        'event_name' => PreProcessEvent::PREPROCESS,
        'event_class' => PreProcessEvent::class,
        'tags' => Tag::WRITE | Tag::READ,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    if ($this->eventClass() === PreProcessEvent::class) {
      $markup = $this->t('This event provides tokens: "[hook]" the preprocess hook id to identify.');
      $form['help'] = [
        '#type' => 'markup',
        '#markup' => $markup,
        '#weight' => 10,
        '#description' => $markup,
      ];
    }
    return $form;
  }

}
