# ECA Helper

## Add helpers functions for ECA

**Events**
- Preprocess event

**Actions**:

- Simple http request
- Workflow label
- Workflow state
- Get `$_SERVER, $_COOKIE, $_SESSION, $_ENV, $_GET, $_POST` variable
- Set cookie value for response.
- Set any form element field value by key
- Get any form element field value by key
- Get/Set ThirtPartySetting
- Get/Set value for variables in preprocess event
- Preprocess attach library
- Preprocess add css class
- Preprocess attach library
- Preprocess remove item


**ECA Quick Action**

Create quick action for ECA without need define plugin and module.

Create file `docroot/sites/eca/EcaActions.php`

```php
<?php

/**
 * Define the ECA quick actions.
 *
 * Define action id, label, and callback, service.
 */
function ECAQuickActions(): array {
  return [
    "hello" => [
      "label" => "ECA Action Hello",
      "callback" => "eca_quick_actions_hello"
    ],
    "hello_inline" => [
      "label" => "ECA Action Inline",
      "callback" => function ($hello) {
        \Drupal::messenger()->addMessage('Inline callback' . $hello);
      }
    ],
    "hello_service" => [
      "label" => "ECA Action Call Drupal service",
      "service" => "messenger",
      "callback" => "addMessage"
    ]
  ];
}

function eca_quick_actions_hello($hello) {
  \Drupal::messenger()->addMessage('Hello world: ' . $hello);
  return "Hello world: " . $hello;
}

```

Use `ECA Helper: Quick Action` and choose the `ECA Action Hello`
